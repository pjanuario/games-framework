﻿using System;
using Games.Framework.Cards;

namespace Games.Framework.Player
{
    /// <summary>
    /// Represents a generic card game player.
    /// </summary>
    public class GamePlayer : IGamePlayer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GamePlayer"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public GamePlayer(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GamePlayer"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="hand">The hand.</param>
        public GamePlayer(string name, IHand hand) : this(name)
        {
            if (hand == null)
                throw new ArgumentNullException("hand");

            Hand = hand;
        }

        /// <summary>
        /// Gets or sets the hand.
        /// </summary>
        /// <value>
        /// The hand.
        /// </value>
        public IHand Hand { get; set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }
    }
}
