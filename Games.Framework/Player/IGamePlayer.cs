using Games.Framework.Cards;

namespace Games.Framework.Player
{
    /// <summary>
    /// Represents the generic game player interface.
    /// </summary>
    public interface IGamePlayer
    {
        /// <summary>
        /// Gets or sets the hand.
        /// </summary>
        /// <value>
        /// The hand.
        /// </value>
        IHand Hand { get; set; }

        /// <summary>
        /// Gets the name.
        /// </summary>
        string Name { get; }
    }
}