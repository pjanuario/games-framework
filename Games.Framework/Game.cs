﻿using System.Collections.Generic;
using Games.Framework.Player;

namespace Games.Framework
{
    /// <summary>
    /// Represents the base abstract Game.
    /// </summary>
    public abstract class Game
    {
        /// <summary>
        /// The list of players.
        /// </summary>
        private readonly List<IGamePlayer> players = new List<IGamePlayer>();

        /// <summary>
        /// The number of players.
        /// </summary>
        private readonly int numberOfPlayers;

        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="numberOfPlayers">The number of players.</param>
        internal Game(string name, int numberOfPlayers)
        {
            Name = name;
            this.numberOfPlayers = numberOfPlayers;
        }

        /// <summary>
        /// Gets the number of players.
        /// </summary>
        public int NumberOfPlayers { get { return numberOfPlayers; } }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the players.
        /// </summary>
        public IList<IGamePlayer> Players { get { return players; } }

        /// <summary>
        /// Ends the of game.
        /// </summary>
        /// <returns></returns>
        protected abstract bool EndOfGame();
    
    }
}
