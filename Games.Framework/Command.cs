﻿using Games.Framework.Blackjack;

namespace Games.Framework
{
    /// <summary>
    /// Represents an abstract command used to implement Command design pattern.
    /// </summary>
    public abstract class Command : ICommand
    {
        /// <summary>
        /// The command receiver.
        /// </summary>
        protected IBlackjackPlayCommandReceiver PlayCommandReceiver;

        /// <summary>
        /// Initializes a new instance of the <see cref="Command"/> class.
        /// </summary>
        /// <param name="receiver">The receiver.</param>
        protected Command(IBlackjackPlayCommandReceiver receiver)
        {
            PlayCommandReceiver = receiver;
        }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        public abstract void Execute();
    }
}
