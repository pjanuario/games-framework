﻿using System;
using System.Collections.Generic;
using System.Linq;
using Games.Framework.Blackjack.Cards;
using Games.Framework.Blackjack.Player;
using Games.Framework.Cards;
using Games.Framework.Player;

namespace Games.Framework.Blackjack
{
    /// <summary>
    /// Represents the Blackjack game motor.
    /// </summary>
    public class BlackjackGame : Game
    {
        /// <summary>
        /// The play command receiver who collects human player data. 
        /// </summary>
        private readonly IBlackjackPlayCommandReceiver playCommandReceiver;

        /// <summary>
        /// The player strategy factories.
        /// </summary>
        private readonly IBlackjackPlayerStrategyFactory strategyFactory;

        /// <summary>
        /// The card deck.
        /// </summary>
        private readonly IDeck deck = new Deck();

        /// <summary>
        /// The Blackjack dealer.
        /// </summary>
        private readonly BlackjackPlayer dealer;

        /// <summary>
        /// The private property that identifies the end of game.
        /// </summary>
        private bool endOfGame;

        #region BlackjackGame State Events

        /// <summary>
        /// The delegate method used to process show cards.
        /// </summary>
        /// <param name="cards">The deck cards.</param>
        public delegate void ShowDeckCards(IEnumerable<PlayingCard> cards);

        /// <summary>
        /// Occurs when the deck is showed..
        /// </summary>
        public event ShowDeckCards ShowDeckCardsEvent = delegate { };

        /// <summary>
        /// The delegate method used to notify that cards are shuffled.
        /// </summary>
        public delegate void DeckShuffled();

        /// <summary>
        /// Occurs when deck is shuffled.
        /// </summary>
        public event DeckShuffled DeckShuffledEvent = delegate { };

        /// <summary>
        /// The delegate method used to notify that PlayerHand is changed.
        /// </summary>
        /// <param name="player">The player.</param>
        public delegate void PlayerHandChanged(IGamePlayer player);

        /// <summary>
        /// Occurs when the player hand changed.
        /// </summary>
        public event PlayerHandChanged PlayerHandChangedEvent = delegate { };

        /// <summary>
        /// The delegate method used to notify that is Player turn.
        /// </summary>
        /// <param name="player">The player.</param>
        public delegate void PlayerTurn(IGamePlayer player);

        /// <summary>
        /// Occurs when is player turn..
        /// </summary>
        public event PlayerTurn PlayerTurnEvent = delegate { };

        /// <summary>
        /// The delegate method used to notify that PlayerResult.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <param name="result">The result.</param>
        public delegate void PlayerResult(IGamePlayer player, BlackjackResult result);

        /// <summary>
        /// Occurs when player result changed.
        /// </summary>
        public event PlayerResult PlayerResultEvent = delegate { };

        /// <summary>
        /// The delegate method used to notify that Game is at the end.
        /// </summary>
        public delegate void GameEnd();

        /// <summary>
        /// Occurs when game end.
        /// </summary>
        public event GameEnd GameEndEvent = delegate { };

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="BlackjackGame"/> class.
        /// </summary>
        /// <param name="receiver">The receiver.</param>
        public BlackjackGame(IBlackjackPlayCommandReceiver receiver)
            : this(1, receiver)
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BlackjackGame"/> class.
        /// </summary>
        /// <param name="numberOfPlayers">The number of players.</param>
        /// <param name="receiver">The receiver.</param>
        public BlackjackGame(int numberOfPlayers, IBlackjackPlayCommandReceiver receiver) : 
            base("Blackjack", numberOfPlayers)
        {
            playCommandReceiver = receiver;
            strategyFactory = new BlackjackPlayerStrategyFactory();

            dealer = new BlackjackPlayer("Dealer", strategyFactory.GetDealerPlayerStrategy());
        }

        /// <summary>
        /// Gets the blackjack players.
        /// </summary>
        private IEnumerable<BlackjackPlayer> BlackjackPlayers
        {
            get { return Players.Cast<BlackjackPlayer>(); }
        }

        /// <summary>
        /// Starts this game instance.
        /// </summary>
        public void Start()
        {
            // Fire show cards event
            ShowDeckCardsEvent(deck.Cards.OrderBy(f => f.Suit).ThenBy(f => f.Value));

            deck.Suffle();
            DeckShuffledEvent();

            InitialDeal();

            // Fire show cards event
            PlayerHandChangedEvent(dealer);

            foreach (var player in BlackjackPlayers)
            {
                // Show initial hand
                PlayerHandChangedEvent(player);

                Deal(player);
            }

            // Turn and play with dealer
            dealer.Hand.Cards.First(c => !c.IsTurned).Turn();

            // Show dealer turned card
            PlayerHandChangedEvent(dealer);

            Deal(dealer);

            foreach (BlackjackPlayer player in BlackjackPlayers)
            {
                BlackjackResult result = player.Win(dealer.BlackjackHand);

                // Show initial hand
                PlayerResultEvent(player, result);
            }

            endOfGame = true;
            GameEndEvent();
        }

        /// <summary>
        /// Inits this instance for a new Game.
        /// </summary>
        public void Init()
        {
            for (int i = 1; i <= NumberOfPlayers; i++)
            {
                string playerName = string.Format("Player {0}", i);
                var player = new BlackjackPlayer(playerName, new BlackjackHand());
                var command = new BlackjackPlayCommand(playCommandReceiver, player);
                player.Strategy = strategyFactory.GetHumanPlayerStrategy(command);
                Players.Add(player);
            }
        }

        /// <summary>
        /// Ends the of game.
        /// </summary>
        /// <returns></returns>
        protected override bool EndOfGame()
        {
            return endOfGame;
        }

        /// <summary>
        /// Initials the deal.
        /// </summary>
        private void InitialDeal()
        {
            dealer.Hand = new BlackjackHand();
            dealer.Hand.AddCard(deck.NextCard().Turn());

            foreach (BlackjackPlayer player in BlackjackPlayers)
            {
                player.Hand = new BlackjackHand();
                player.Hand.AddCard(deck.NextCard().Turn());
                player.Hand.AddCard(deck.NextCard().Turn());
            }

            dealer.Hand.AddCard(deck.NextCard());
        }

        /// <summary>
        /// Deals the cards for the specified player.
        /// </summary>
        /// <param name="player">The player.</param>
        private void Deal(BlackjackPlayer player)
        {
            PlayerTurnEvent(player);

            BlackjackPlayerDecision decision;

            while (
                !player.BlackjackHand.IsBusted()
                && player.BlackjackHand.IsBelow()
                && (decision = player.Play()) != BlackjackPlayerDecision.Stand)
            {
                // check for unsupported decisions
                bool unsupportedDecision = decision == BlackjackPlayerDecision.DoubleDown
                                           || decision == BlackjackPlayerDecision.Split
                                           || decision == BlackjackPlayerDecision.Surrender;

                if (unsupportedDecision)
                    throw new ApplicationException("Unsupported decision!");

                player.Hand.AddCard(deck.NextCard().Turn());
                PlayerHandChangedEvent(player);
            }
        }        
    
    }
}
