using Games.Framework.Cards;

namespace Games.Framework.Blackjack.Cards
{
    /// <summary>
    /// Defines a generic Blackjack hand. 
    /// It uses Decorator to extend base interface <see cref="IHand"/>.
    /// </summary>
    internal interface IBlackjackHand : IHand
    {
        /// <summary>
        /// Determines whether this instance is blackjack.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is blackjack; otherwise, <c>false</c>.
        /// </returns>
        bool IsBlackjack();

        /// <summary>
        /// Determines whether this instance is busted.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is busted; otherwise, <c>false</c>.
        /// </returns>
        bool IsBusted();

        /// <summary>
        /// Determines whether this instance is below.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is below; otherwise, <c>false</c>.
        /// </returns>
        bool IsBelow();
    }
}