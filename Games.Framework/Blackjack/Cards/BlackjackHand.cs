﻿using System.Linq;
using Games.Framework.Cards;

namespace Games.Framework.Blackjack.Cards
{
    /// <summary>
    /// Represents a Blackjack hand..
    /// </summary>
    class BlackjackHand : Hand, IBlackjackHand
    {
        /// <summary>
        /// Defines the Blackjack default limit.
        /// </summary>
        public const int Blackjack = 21;

        /// <summary>
        /// Gets the hand value.
        /// </summary>
        /// <returns>the hand value</returns>
        public override int GetValue()
        {
            var softHand = Cards.Where(c => !c.IsFace() && c.IsTurned).Sum(c => c.Value)
                + Cards.Count(c => c.IsFace() && c.IsTurned) * 10;

            var hardHand = softHand + Cards.Count(c => c.IsAce() && c.IsTurned) * 10;

            return hardHand > Blackjack ? softHand : hardHand;
        }

        /// <summary>
        /// Determines whether this instance is blackjack.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is blackjack; otherwise, <c>false</c>.
        /// </returns>
        public bool IsBlackjack()
        {
            if (GetValue() != Blackjack) 
                return false;

            return Cards.Count() == 2 
                && Cards.Count(c => c.IsAce() && c.IsTurned) == 1 
                && Cards.Count(c => c.IsFace() && c.IsTurned) == 1;
        }

        /// <summary>
        /// Determines whether this instance is busted.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is busted; otherwise, <c>false</c>.
        /// </returns>
        public bool IsBusted()
        {
            return GetValue() > Blackjack;
        }

        /// <summary>
        /// Determines whether this instance is below.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is below; otherwise, <c>false</c>.
        /// </returns>
        public bool IsBelow()
        {
            return GetValue() < Blackjack;
        }
    }
}
