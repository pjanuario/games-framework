using Games.Framework.Blackjack.Player;
using Games.Framework.Player;

namespace Games.Framework.Blackjack
{
    /// <summary>
    /// Represents the Blackjack Game play comm.
    /// </summary>
    public class BlackjackPlayCommand : Command, IBlackjackPlayCommand
    {
        /// <summary>
        /// Gets the player.
        /// </summary>
        public IGamePlayer Player { get; private set; }

        /// <summary>
        /// Gets or sets the player decision.
        /// </summary>
        /// <value>
        /// The player decision.
        /// </value>
        public BlackjackPlayerDecision PlayerDecision { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BlackjackPlayCommand"/> class.
        /// </summary>
        /// <param name="receiver">The receiver.</param>
        /// <param name="player">The player.</param>
        public BlackjackPlayCommand(IBlackjackPlayCommandReceiver receiver, IGamePlayer player) 
            : base(receiver)
        {
            Player = player;
        }

        /// <summary>
        /// Executes this command instance.
        /// </summary>
        public override void Execute()
        {
            PlayerDecision = PlayCommandReceiver.GetPlayDecision(Player);
        }
    }
}