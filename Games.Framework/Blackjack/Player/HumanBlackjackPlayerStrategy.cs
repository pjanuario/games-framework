﻿using Games.Framework.Blackjack.Cards;

namespace Games.Framework.Blackjack.Player
{
    /// <summary>
    /// Represents the human player strategy.
    /// </summary>
    class HumanBlackjackPlayerStrategy : IBlackjackPlayerStrategy
    {
        /// <summary>
        /// The command used to execute player play on is turn.
        /// </summary>
        private readonly IBlackjackPlayCommand command;

        /// <summary>
        /// Initializes a new instance of the <see cref="HumanBlackjackPlayerStrategy"/> class.
        /// </summary>
        /// <param name="command">The command used to get player decision on turn.</param>
        public HumanBlackjackPlayerStrategy(IBlackjackPlayCommand command)
        {
            this.command = command;
        }

        /// <summary>
        /// Plays and takes the decision with the specified hand.
        /// </summary>
        /// <param name="hand">The hand.</param>
        /// <returns></returns>
        public BlackjackPlayerDecision Play(IBlackjackHand hand)
        {
            command.Execute();
            return command.PlayerDecision;
        }
    }
}
