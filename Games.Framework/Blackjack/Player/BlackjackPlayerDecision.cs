﻿namespace Games.Framework.Blackjack.Player
{
    /// <summary>
    /// After receiving an initial two cards, the player has up to four standard options: "hit," "stand," "double down," or "split". 
    /// Each option has a corresponding hand signal. Some games give the player a fifth option, "surrender".
    /// </summary>
    public enum BlackjackPlayerDecision
    {
        /// <summary>
        /// Take another card from the dealer.
        ///    signal: (handheld) Scrape cards against table. (face up) Tap the table or wave hand toward body.
        /// </summary>
        Hit,

        /// <summary>
        /// Take no more cards; also known as "stand pat", "stick", or "stay".
        ///    signal: (handheld) Slide cards under chips. (face up) Wave hand horizontally.
        /// </summary>
        Stand,

        /// <summary>
        /// The player is allowed to increase the initial bet by up to 100% in exchange for committing to stand after receiving 
        /// exactly one more card. The additional bet is placed in the betting box next to the original bet. Some games do not 
        /// permit the player to increase the bet by amounts other than 100%. Non-controlling players may double their wager or 
        /// decline to do so, but they are bound by the controlling player's decision to take only one card.
        ///    signal: Place additional chips beside the original bet, and point with one finger.
        /// </summary>
        DoubleDown,
        
        /// <summary>
        /// This decision is only available as first decision of a hand. 
        /// If the first two cards have the same value, the player can split them into two hands, by moving a second bet equal to the 
        /// first into an area outside the betting box of the original bet. The dealer separates the two cards and draws a further card 
        /// on each, placing one bet with each hand. 
        /// The player then plays out the two separate hands in turn, with some restrictions. Occasionally, in the case of ten-valued 
        /// cards, some casinos allow splitting only when the cards have the identical ranks; for instance, a hand of T-T may be split, 
        /// but not of T-K. However, usually all ten-value cards are treated the same. Doubling and further splitting of post-split 
        /// hands may be restricted, and blackjacks after a split are counted as non-blackjack 21 when comparing against the dealer's 
        /// hand. Hitting split aces is usually not allowed. Non-controlling players may follow the controlling player by putting 
        /// down an additional bet, or decline to do so, instead associating their existing wager with one of the two post-split hands. 
        /// In that case they must choose which hand to play behind before the second cards are drawn.
        ///    signal: Place additional chips next to the original bet outside the betting box. Point with two fingers spread into 
        ///            a V formation.
        /// </summary>
        Split,

        /// <summary>
        /// This decision is only available as first decision of a hand. 
        /// Some games offer the option to "surrender", usually in hole card games and directly after the dealer has checked for 
        /// blackjack (but see below for variations). When the player surrenders, the house takes half the player's bet and return 
        /// the other half to the player; this terminates the player's interest in the hand. The request to surrender is made verbally, 
        /// there being no standard hand signal.
        /// </summary>
        Surrender
    }
}
