﻿using Games.Framework.Blackjack.Cards;

namespace Games.Framework.Blackjack.Player
{
    /// <summary>
    /// Represents a dealer player stategy.
    /// </summary>
    class DealerBlackjackPlayerStrategy : IBlackjackPlayerStrategy
    {
        /// <summary>
        /// The minimum value that the dealer need to have.
        /// </summary>
        private const int MinValue = 16;

        /// <summary>
        /// Plays and takes the decision based on the specified hand.
        /// </summary>
        /// <param name="hand">The hand.</param>
        /// <returns></returns>
        public BlackjackPlayerDecision Play(IBlackjackHand hand)
        {
            return (hand.GetValue() <= MinValue) ? BlackjackPlayerDecision.Hit : BlackjackPlayerDecision.Stand;
        }
    }
}
