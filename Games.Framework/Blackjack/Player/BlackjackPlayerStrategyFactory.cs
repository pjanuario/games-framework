namespace Games.Framework.Blackjack.Player
{
    /// <summary>
    /// Creates available strategies to blackjack players.
    /// </summary>
    class BlackjackPlayerStrategyFactory : IBlackjackPlayerStrategyFactory
    {
        /// <summary>
        /// Gets the human player strategy.
        /// </summary>
        /// <param name="command">The command used to get player decision on turn.</param>
        /// <returns>the generic strategy</returns>
        public IBlackjackPlayerStrategy GetHumanPlayerStrategy(IBlackjackPlayCommand command)
        {
            return new HumanBlackjackPlayerStrategy(command);
        }

        /// <summary>
        /// Gets the dealer player strategy.
        /// </summary>
        /// <returns>the generic strategy</returns>
        public IBlackjackPlayerStrategy GetDealerPlayerStrategy()
        {
            return new DealerBlackjackPlayerStrategy();
        }
    }
}