﻿using Games.Framework.Blackjack.Cards;

namespace Games.Framework.Blackjack.Player
{
    /// <summary>
    /// Represents the generic interface for players strategies.
    /// </summary>
    interface IBlackjackPlayerStrategy
    {
        /// <summary>
        /// The command used to get player decision on turn.
        /// </summary>
        /// <param name="hand">The hand.</param>
        /// <returns></returns>
        BlackjackPlayerDecision Play(IBlackjackHand hand);
    }
}
