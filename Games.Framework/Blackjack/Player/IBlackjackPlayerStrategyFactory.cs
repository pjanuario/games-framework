namespace Games.Framework.Blackjack.Player
{
    /// <summary>
    /// Represents the generic Blackjack player factory.
    /// </summary>
    internal interface IBlackjackPlayerStrategyFactory
    {
        /// <summary>
        /// Gets the human player strategy.
        /// </summary>
        /// <param name="command">The command used to get player decision on turn.</param>
        /// <returns></returns>
        IBlackjackPlayerStrategy GetHumanPlayerStrategy(IBlackjackPlayCommand command);

        /// <summary>
        /// Gets the dealer player strategy.
        /// </summary>
        /// <returns></returns>
        IBlackjackPlayerStrategy GetDealerPlayerStrategy();
    }
}