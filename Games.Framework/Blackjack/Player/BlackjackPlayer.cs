﻿using System;
using Games.Framework.Blackjack.Cards;
using Games.Framework.Player;

namespace Games.Framework.Blackjack.Player
{
    /// <summary>
    /// Represents a Blackjack game player.
    /// </summary>
    class BlackjackPlayer : GamePlayer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BlackjackPlayer"/> class.
        /// </summary>
        /// <param name="name">The player name.</param>
        public BlackjackPlayer(string name) : base(name) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="BlackjackPlayer"/> class.
        /// </summary>
        /// <param name="name">The player name.</param>
        /// <param name="hand">The blackjack hand.</param>
        public BlackjackPlayer(string name, IBlackjackHand hand) : base(name, hand) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="BlackjackPlayer"/> class.
        /// </summary>
        /// <param name="name">The player name.</param>
        /// <param name="strategy">The blackjack player strategy.</param>
        public BlackjackPlayer(string name, IBlackjackPlayerStrategy strategy)
            : base(name)
        {
            if (strategy == null) throw new ArgumentNullException("strategy");

            Strategy = strategy;
        }

        /// <summary>
        /// Gets or sets the player strategy.
        /// </summary>
        /// <value>
        /// The player strategy.
        /// </value>
        public IBlackjackPlayerStrategy Strategy { get; set; }

        /// <summary>
        /// Gets the blackjack hand.
        /// </summary>
        public IBlackjackHand BlackjackHand
        {
            get { return Hand as IBlackjackHand; }
        }

        /// <summary>
        /// Execute player play.
        /// </summary>
        /// <returns>the player decision on play.</returns>
        public BlackjackPlayerDecision Play()
        {
            if(Strategy == null) 
                throw new ApplicationException("The player doesn't have an associated strategy!");

            return Strategy.Play(BlackjackHand);
        }

        /// <summary>
        /// Compare and check if the player Wins the specified other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns>the player result.</returns>
        public BlackjackResult Win(IBlackjackHand other)
        {
            if (BlackjackHand.IsBlackjack())
                return other.IsBlackjack() ? BlackjackResult.Push : BlackjackResult.Win;

            if (other.IsBlackjack())
                return BlackjackResult.Loose;

            if (BlackjackHand.IsBusted())
                return other.IsBusted() ? BlackjackResult.Push : BlackjackResult.Loose;

            if (other.IsBusted())
                return BlackjackResult.Win;

            if (BlackjackHand.GetValue() == other.GetValue())
                return BlackjackResult.Push;

            return BlackjackHand.GetValue() > other.GetValue() ? BlackjackResult.Win : BlackjackResult.Loose;
        }
    }
}
