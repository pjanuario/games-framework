namespace Games.Framework.Blackjack
{
    /// <summary>
    /// Represents the Blackjack possible results.
    /// </summary>
    public enum BlackjackResult
    {
        /// <summary>
        /// The player loose.
        /// </summary>
        Loose,

        /// <summary>
        /// The player win.
        /// </summary>
        Win,

        /// <summary>
        /// The tie.
        /// </summary>
        Push
    }
}