using Games.Framework.Blackjack.Player;
using Games.Framework.Player;

namespace Games.Framework.Blackjack
{
    /// <summary>
    /// Represents a generic Blackjack play command receiver.
    /// </summary>
    public interface IBlackjackPlayCommandReceiver
    {
        /// <summary>
        /// Gets the play decision.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns>the <see cref="BlackjackPlayerDecision"/></returns>
        BlackjackPlayerDecision GetPlayDecision(IGamePlayer player);
    }
}