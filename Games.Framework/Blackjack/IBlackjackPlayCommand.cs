using Games.Framework.Blackjack.Player;
using Games.Framework.Player;

namespace Games.Framework.Blackjack
{
    /// <summary>
    /// Represents a generic interface for Blackjack play commands.
    /// </summary>
    public interface IBlackjackPlayCommand
    {
        /// <summary>
        /// Gets the player.
        /// </summary>
        IGamePlayer Player { get; }

        /// <summary>
        /// Gets or sets the player decision.
        /// </summary>
        /// <value>
        /// The player decision.
        /// </value>
        BlackjackPlayerDecision PlayerDecision { get; set; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        void Execute();
    }
}