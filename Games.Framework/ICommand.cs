namespace Games.Framework
{
    /// <summary>
    /// The command interface used to implement Command pattern.
    /// </summary>
    public interface ICommand
    {
        /// <summary>
        /// Executes this instance.
        /// </summary>
        void Execute();
    }
}