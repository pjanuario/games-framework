using System.Collections.Generic;

namespace Games.Framework.Cards
{
    /// <summary>
    /// Represents the generic card game hand interface.
    /// </summary>
    public interface IHand
    {
        /// <summary>
        /// Gets the cards.
        /// </summary>
        IEnumerable<PlayingCard> Cards { get; }

        /// <summary>
        /// Gets the number of cards.
        /// </summary>
        int NumberOfCards { get; }

        /// <summary>
        /// Adds the card.
        /// </summary>
        /// <param name="card">The card.</param>
        void AddCard(PlayingCard card);

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <returns></returns>
        int GetValue();
    }
}