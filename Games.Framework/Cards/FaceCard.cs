namespace Games.Framework.Cards
{
    /// <summary>
    /// Represents a Face card.
    /// </summary>
    class FaceCard : PlayingCard
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FaceCard"/> class.
        /// </summary>
        /// <param name="suit">The suit.</param>
        /// <param name="value">The value.</param>
        public FaceCard(CardSuit suit, int value) : base(suit, value) {
        
        }

        /// <summary>
        /// Determines whether this instance is face.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is face; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsFace()
        {
            return true;
        }
    }

}