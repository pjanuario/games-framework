namespace Games.Framework.Cards
{
    /// <summary>
    /// Represents the numeric cards.
    /// </summary>
    class NumericCard : PlayingCard
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NumericCard"/> class.
        /// </summary>
        /// <param name="suit">The suit.</param>
        /// <param name="number">The number.</param>
        public NumericCard(CardSuit suit, int number) 
            : base(suit, number)
        {

        }
    }
}