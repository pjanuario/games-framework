﻿using System;
using System.Collections.Generic;

namespace Games.Framework.Cards
{
    /// <summary>
    /// Represents an abstract hand, used as base for multiple games hands.
    /// </summary>
    abstract class Hand : IHand
    {
        /// <summary>
        /// The list of cards in the hand.
        /// </summary>
        private readonly List<PlayingCard> cards;

        /// <summary>
        /// Initializes a new instance of the <see cref="Hand"/> class.
        /// </summary>
        protected Hand()
        {
            cards = new List<PlayingCard>();
        }

        /// <summary>
        /// Gets the cards.
        /// </summary>
        public IEnumerable<PlayingCard> Cards { get { return cards.AsReadOnly(); } }

        /// <summary>
        /// Gets the number of cards.
        /// </summary>
        public int NumberOfCards { get { return cards.Count; } }

        /// <summary>
        /// Adds the card.
        /// </summary>
        /// <param name="card">The card.</param>
        public void AddCard(PlayingCard card)
        {
            if(card == null)
                throw new ArgumentNullException("card");

            cards.Add(card);
        }

        /// <summary>
        /// Gets the hand value.
        /// </summary>
        /// <returns></returns>
        public abstract int GetValue();
    }
}
