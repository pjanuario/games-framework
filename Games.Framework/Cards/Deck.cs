﻿using System;
using System.Collections.Generic;
using Games.Framework.Cards.Algorithm;

namespace Games.Framework.Cards
{
    /// <summary>
    /// Represents a Casino card deck.
    /// </summary>
    class Deck : IDeck
    {
        /// <summary>
        /// The deck default size.
        /// </summary>
        private const int DeckSize = 52;

        /// <summary>
        /// The list of <see cref="PlayingCard"/>
        /// </summary>
        private readonly List<PlayingCard> cards = new List<PlayingCard>(52);

        /// <summary>
        /// The next card list index.
        /// </summary>
        private int nextCardIndex;

        /// <summary>
        /// The strategy used for shuffle.
        /// </summary>
        private readonly IShuffleAlgorithm<PlayingCard> shuffleStrategy;

        /// <summary>
        /// Initializes a new instance of the <see cref="Deck"/> class.
        /// Uses <see cref="SimpleShuffleAlgorithm{T}"/> as suffling algorithm.
        /// </summary>
        public Deck()
            : this(new SimpleShuffleAlgorithm<PlayingCard>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Deck"/> class.
        /// </summary>
        /// <param name="shuffleStrategy">The shuffle strategy.</param>
        public Deck(IShuffleAlgorithm<PlayingCard> shuffleStrategy)
        {
            this.shuffleStrategy = shuffleStrategy;

            PlayingCard.CardSuit[] suites = new[]{ 
                PlayingCard.CardSuit.Clubs, 
                PlayingCard.CardSuit.Diamonds, 
                PlayingCard.CardSuit.Hearts, 
                PlayingCard.CardSuit.Spades
            };

            foreach (PlayingCard.CardSuit suit in suites)
            {
                cards.Add(new AceCard(suit));

                for (int number = 2; number <= 10; number++)
                    cards.Add(new NumericCard(suit, number));

                cards.Add(new FaceCard(suit, 11));
                cards.Add(new FaceCard(suit, 12));
                cards.Add(new FaceCard(suit, 13));
            }
        }

        /// <summary>
        /// Gets the cards present on the deck.
        /// </summary>
        public IEnumerable<PlayingCard> Cards
        {
            get { return cards.AsReadOnly(); }
        }

        /// <summary>
        /// Suffles this instance.
        /// </summary>
        public void Suffle()
        {
            shuffleStrategy.Shuffle(cards);
            nextCardIndex = 0;
        }

        /// <summary>
        /// Get the next card from shuffle.
        /// </summary>
        /// <returns></returns>
        public PlayingCard NextCard()
        {
            if (nextCardIndex == DeckSize)
                throw new ApplicationException("The deck reach the end!");

            return cards[nextCardIndex++];
        }

        /// <summary>
        /// Gets the total cards left on shuffle.
        /// </summary>
        public int TotalCardsLeft
        {
            get { return DeckSize - nextCardIndex; }
        }
    }
}
