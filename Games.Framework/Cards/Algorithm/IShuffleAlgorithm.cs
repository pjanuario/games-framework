using System.Collections.Generic;

namespace Games.Framework.Cards.Algorithm
{
    /// <summary>
    /// Defines a generalized suffle algorithm.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IShuffleAlgorithm<T>
    {
        /// <summary>
        /// Shuffles the specified items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">The items.</param>
        void Shuffle<T>(IList<T> items);
    }
}