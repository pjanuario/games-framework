﻿using System;
using System.Collections.Generic;

namespace Games.Framework.Cards.Algorithm
{
    /// <summary>
    /// Represents a simple shuffle algorithm.
    /// </summary>
    /// <typeparam name="T">the item type to suffle</typeparam>
    public class SimpleShuffleAlgorithm<T> : IShuffleAlgorithm<T>
    {
        /// <summary>
        /// Shuffles the specified items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items">The items.</param>
        public void Shuffle<T>(IList<T> items)
        {
            var randomGenerator = new Random();
            var itemIndex = items.Count;

            while (itemIndex > 1)
            {
                var randomIndex = (randomGenerator.Next() % itemIndex);
                itemIndex--;

                //Swap the indexed and random positions
                T value = items[randomIndex];
                items[randomIndex] = items[itemIndex];
                items[itemIndex] = value;
            }
        }
    }
}
