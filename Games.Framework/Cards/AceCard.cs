namespace Games.Framework.Cards
{
    /// <summary>
    /// Represents an Ace playng card.
    /// </summary>
    class AceCard : PlayingCard
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AceCard"/> class.
        /// </summary>
        /// <param name="suit">The suit.</param>
        public AceCard(CardSuit suit)
            : base(suit, 1)
        {
        }

        /// <summary>
        /// Determines whether this instance is ace.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is ace; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsAce()
        {
            return true;
        }
    }
}