using System.Collections.Generic;

namespace Games.Framework.Cards
{
    /// <summary>
    /// Represents a generic interface for a deck card game.
    /// </summary>
    interface IDeck
    {
        /// <summary>
        /// Gets the list of cards.
        /// </summary>
        IEnumerable<PlayingCard> Cards { get; }

        /// <summary>
        /// Gets the total cards left.
        /// </summary>
        int TotalCardsLeft { get; }

        /// <summary>
        /// Suffles this instance.
        /// </summary>
        void Suffle();

        /// <summary>
        /// Nexts the card.
        /// </summary>
        /// <returns>The next <see cref="PlayingCard"/>.</returns>
        PlayingCard NextCard();
    }
}