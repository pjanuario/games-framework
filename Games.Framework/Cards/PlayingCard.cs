﻿using System;

namespace Games.Framework.Cards
{
    /// <summary>
    /// Represents the abstract playing card.
    /// </summary>
    public abstract class PlayingCard : IEquatable<PlayingCard>
    {
        /// <summary>
        /// Represents the playing card suite.
        /// </summary>
        public enum CardSuit
        {
            Hearts,
            Spades,
            Diamonds,
            Clubs
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayingCard"/> class.
        /// </summary>
        /// <param name="suit">The suit.</param>
        /// <param name="value">The value.</param>
        protected PlayingCard(CardSuit suit, int value)
        {
            Value = value;
            Suit = suit;
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        public int Value { get; private set; }

        /// <summary>
        /// Gets the suit.
        /// </summary>
        public CardSuit Suit { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance is turned.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is turned; otherwise, <c>false</c>.
        /// </value>
        public bool IsTurned { get; private set; }

        /// <summary>
        /// Turns this instance.
        /// </summary>
        /// <returns>the <c>this</c></returns>
        public PlayingCard Turn()
        {
            IsTurned = true;
            return this;
        }

        /// <summary>
        /// Determines whether this instance is ace.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is ace; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool IsAce() { return false;  }

        /// <summary>
        /// Determines whether this instance is face.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is face; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool IsFace() { return false;  }

        #region Equality Members

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        public bool Equals(PlayingCard other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other.Value == Value && Equals(other.Suit, Suit);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof (PlayingCard)) return false;
            return Equals((PlayingCard) obj);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return (Value*397) ^ Suit.GetHashCode();
            }
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(PlayingCard left, PlayingCard right)
        {
            return Equals(left, right);
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(PlayingCard left, PlayingCard right)
        {
            return !Equals(left, right);
        }

        #endregion
    }
}
