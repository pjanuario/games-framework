﻿using System;
using Games.Framework.Blackjack;
using Games.Framework.Blackjack.Cards;
using Games.Framework.Blackjack.Player;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Games.Tests.Blackjack.Player
{
    [TestClass]
    public class BlackjackPlayerUnitTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestIfBlackplayerCtorCheckNullStrategy()
        {
            // Act
            new BlackjackPlayer(string.Empty, (IBlackjackPlayerStrategy)null);

            // Assert
            // Expect exception
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestIfBlackplayerCtorCheckNullHand()
        {
            // Act
            new BlackjackPlayer(string.Empty, (IBlackjackHand)null);

            // Assert
            // Expect exception
        }

        #region Win Test

        [TestMethod]
        public void TestPlayerWinWithBlackjack()
        {
            // Arrange
            var otherHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            otherHandMock.Setup(f => f.IsBlackjack()).Returns(() => false);

            var playerHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            playerHandMock.Setup(f => f.IsBlackjack()).Returns(() => true);

            var strategyMock = new Mock<IBlackjackPlayerStrategy>(MockBehavior.Strict);
            strategyMock.Setup(t => t.Play(It.IsAny<IBlackjackHand>())).Verifiable();

            var target = new BlackjackPlayer(string.Empty, playerHandMock.Object);

            // Act
            var actual = target.Win(otherHandMock.Object);

            //Assert
            Assert.AreEqual(BlackjackResult.Win, actual);
        }

        [TestMethod]
        public void TestPlayerWinWithHigherHand()
        {
            // Arrange
            var otherHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            otherHandMock.Setup(f => f.IsBlackjack()).Returns(() => false);
            otherHandMock.Setup(f => f.IsBusted()).Returns(() => false);
            otherHandMock.Setup(f => f.GetValue()).Returns(() => 18);

            var playerHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            playerHandMock.Setup(f => f.IsBusted()).Returns(() => false);
            playerHandMock.Setup(f => f.IsBlackjack()).Returns(() => false);
            playerHandMock.Setup(f => f.GetValue()).Returns(() => 19);

            var strategyMock = new Mock<IBlackjackPlayerStrategy>(MockBehavior.Strict);
            strategyMock.Setup(t => t.Play(It.IsAny<IBlackjackHand>())).Verifiable();

            var target = new BlackjackPlayer(string.Empty, playerHandMock.Object);

            // Act
            var actual = target.Win(otherHandMock.Object);

            //Assert
            Assert.AreEqual(BlackjackResult.Win, actual);
        }

        [TestMethod]
        public void TestPlayerLooseWithLowerHand()
        {
            // Arrange
            var otherHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            otherHandMock.Setup(f => f.IsBlackjack()).Returns(() => false);
            otherHandMock.Setup(f => f.IsBusted()).Returns(() => false);
            otherHandMock.Setup(f => f.GetValue()).Returns(() => 20);

            var playerHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            playerHandMock.Setup(f => f.IsBlackjack()).Returns(() => false);
            playerHandMock.Setup(f => f.IsBusted()).Returns(() => false);
            playerHandMock.Setup(f => f.GetValue()).Returns(() => 19);

            var strategyMock = new Mock<IBlackjackPlayerStrategy>(MockBehavior.Strict);
            strategyMock.Setup(t => t.Play(It.IsAny<IBlackjackHand>())).Verifiable();

            var target = new BlackjackPlayer(string.Empty, playerHandMock.Object);

            // Act
            var actual = target.Win(otherHandMock.Object);

            //Assert
            Assert.AreEqual(BlackjackResult.Loose, actual);
        }

        [TestMethod]
        public void TestPlayerLooseAgainstBlackjackHand()
        {
            // Arrange
            var otherHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            otherHandMock.Setup(f => f.IsBlackjack()).Returns(() => true);

            var playerHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            playerHandMock.Setup(f => f.IsBlackjack()).Returns(() => false);
            playerHandMock.Setup(f => f.GetValue()).Returns(() => 19);

            var strategyMock = new Mock<IBlackjackPlayerStrategy>(MockBehavior.Strict);
            strategyMock.Setup(t => t.Play(It.IsAny<IBlackjackHand>())).Verifiable();

            var target = new BlackjackPlayer(string.Empty, playerHandMock.Object);

            // Act
            var actual = target.Win(otherHandMock.Object);

            //Assert
            Assert.AreEqual(BlackjackResult.Loose, actual);
        }

        [TestMethod]
        public void TestPlayerLooseWithBustHand()
        {
            // Arrange
            var otherHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            otherHandMock.Setup(f => f.IsBlackjack()).Returns(() => false);
            otherHandMock.Setup(f => f.IsBusted()).Returns(() => false);
            otherHandMock.Setup(f => f.GetValue()).Returns(() => 17);

            var playerHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            playerHandMock.Setup(f => f.IsBlackjack()).Returns(() => false);
            playerHandMock.Setup(f => f.IsBusted()).Returns(() => true);

            var strategyMock = new Mock<IBlackjackPlayerStrategy>(MockBehavior.Strict);
            strategyMock.Setup(t => t.Play(It.IsAny<IBlackjackHand>())).Verifiable();

            var target = new BlackjackPlayer(string.Empty, playerHandMock.Object);

            // Act
            var actual = target.Win(otherHandMock.Object);

            //Assert
            Assert.AreEqual(BlackjackResult.Loose, actual);
        }

        [TestMethod]
        public void TestPlayerPushWithBlackjackHand()
        {
            // Arrange
            var otherHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            otherHandMock.Setup(f => f.IsBlackjack()).Returns(() => true);

            var playerHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            playerHandMock.Setup(f => f.IsBlackjack()).Returns(() => true);

            var strategyMock = new Mock<IBlackjackPlayerStrategy>(MockBehavior.Strict);
            strategyMock.Setup(t => t.Play(It.IsAny<IBlackjackHand>())).Verifiable();

            var target = new BlackjackPlayer(string.Empty, playerHandMock.Object);

            // Act
            var actual = target.Win(otherHandMock.Object);

            //Assert
            Assert.AreEqual(BlackjackResult.Push, actual);
        }

        [TestMethod]
        public void TestPlayerPushWithRegularHand()
        {
            // Arrange
            var otherHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            otherHandMock.Setup(f => f.IsBusted()).Returns(() => false);
            otherHandMock.Setup(f => f.IsBlackjack()).Returns(() => false);
            otherHandMock.Setup(f => f.GetValue()).Returns(() => 20);

            var playerHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            playerHandMock.Setup(f => f.IsBusted()).Returns(() => false);
            playerHandMock.Setup(f => f.IsBlackjack()).Returns(() => false);
            playerHandMock.Setup(f => f.GetValue()).Returns(() => 20);

            var strategyMock = new Mock<IBlackjackPlayerStrategy>(MockBehavior.Strict);
            strategyMock.Setup(t => t.Play(It.IsAny<IBlackjackHand>())).Verifiable();

            var target = new BlackjackPlayer(string.Empty, playerHandMock.Object);

            // Act
            var actual = target.Win(otherHandMock.Object);

            //Assert
            Assert.AreEqual(BlackjackResult.Push, actual);
        }

        [TestMethod]
        public void TestPlayerPushWithBustedHand()
        {
            // Arrange
            var otherHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            otherHandMock.Setup(f => f.IsBlackjack()).Returns(() => false);
            otherHandMock.Setup(f => f.IsBusted()).Returns(() => true);

            var playerHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            playerHandMock.Setup(f => f.IsBlackjack()).Returns(() => false);
            playerHandMock.Setup(f => f.IsBusted()).Returns(() => true);

            var strategyMock = new Mock<IBlackjackPlayerStrategy>(MockBehavior.Strict);
            strategyMock.Setup(t => t.Play(It.IsAny<IBlackjackHand>())).Verifiable();

            var target = new BlackjackPlayer(string.Empty, playerHandMock.Object);

            // Act
            var actual = target.Win(otherHandMock.Object);

            //Assert
            Assert.AreEqual(BlackjackResult.Push, actual);
        }

        [TestMethod]
        public void TestPlayerWithOponentBustedHand()
        {
            // Arrange
            var otherHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            otherHandMock.Setup(f => f.IsBlackjack()).Returns(() => false);
            otherHandMock.Setup(f => f.IsBusted()).Returns(() => true);

            var playerHandMock = new Mock<IBlackjackHand>(MockBehavior.Strict);
            playerHandMock.Setup(f => f.IsBlackjack()).Returns(() => false);
            playerHandMock.Setup(f => f.IsBusted()).Returns(() => false);

            var strategyMock = new Mock<IBlackjackPlayerStrategy>(MockBehavior.Strict);
            strategyMock.Setup(t => t.Play(It.IsAny<IBlackjackHand>())).Verifiable();

            var target = new BlackjackPlayer(string.Empty, playerHandMock.Object);

            // Act
            var actual = target.Win(otherHandMock.Object);

            //Assert
            Assert.AreEqual(BlackjackResult.Win, actual);
        }

        #endregion

        [TestMethod]
        public void TestIfStrategyIsExecuted()
        {
            // Arrange
            var strategyMock = new Mock<IBlackjackPlayerStrategy>(MockBehavior.Strict);
            strategyMock
                .Setup(t => t.Play(It.IsAny<IBlackjackHand>()))
                .Returns(() => BlackjackPlayerDecision.Hit)
                .Verifiable();

            var strategy = strategyMock.Object;
            var target = new BlackjackPlayer(string.Empty, strategy);

            // Act
            target.Play();

            // Assert
            strategyMock.Verify();
        }
    }
}
