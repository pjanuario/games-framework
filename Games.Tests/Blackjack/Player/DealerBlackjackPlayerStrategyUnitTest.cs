﻿using Games.Framework.Blackjack;
using Games.Framework.Blackjack.Cards;
using Games.Framework.Blackjack.Player;
using Games.Framework.Cards;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Games.Tests.Blackjack.Player
{
    [TestClass]
    public class DealerBlackjackPlayerStrategyUnitTest
    {
        [TestMethod]
        public void TestIfDealerHitWithHandLowerThan17()
        {
            // Arrange
            IBlackjackPlayerStrategy target = new DealerBlackjackPlayerStrategy();
            var hand = new BlackjackHand();
            hand.AddCard(new AceCard(PlayingCard.CardSuit.Clubs).Turn());
            hand.AddCard(new NumericCard(PlayingCard.CardSuit.Clubs, 5).Turn());

            // Act
            var actual = target.Play(hand);

            // Assert
            Assert.AreEqual(BlackjackPlayerDecision.Hit, actual);
        }

        [TestMethod]
        public void TestIfDealerStandWithHandHigherThan16()
        {
            // Arrange
            IBlackjackPlayerStrategy target = new DealerBlackjackPlayerStrategy();
            var hand = new BlackjackHand();
            hand.AddCard(new AceCard(PlayingCard.CardSuit.Clubs).Turn());
            hand.AddCard(new NumericCard(PlayingCard.CardSuit.Clubs, 6).Turn());

            // Act
            var actual = target.Play(hand);

            // Assert
            Assert.AreEqual(BlackjackPlayerDecision.Stand, actual);
        }
    }
}
