﻿using Games.Framework;
using Games.Framework.Blackjack;
using Games.Framework.Blackjack.Player;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Games.Tests.Blackjack.Player
{
    [TestClass]
    public class BlackjackPlayerStrategyFactoryUnitTest
    {
        [TestMethod]
        public void TestIfGetDealerPlayerStrategyReturnDealerStrategy()
        {
            // Arrange
            var target = new BlackjackPlayerStrategyFactory();

            // Act
            var actual = target.GetDealerPlayerStrategy();

            // Assert
            Assert.IsInstanceOfType(actual, typeof(DealerBlackjackPlayerStrategy));
        }

        [TestMethod]
        public void TestIfGetHumanPlayerStrategyReturnHumanStrategy()
        {
            // Arrange
            var target = new BlackjackPlayerStrategyFactory();
            var command = new Mock<IBlackjackPlayCommand>(MockBehavior.Strict).Object;

            // Act
            var actual = target.GetHumanPlayerStrategy(command);

            // Assert
            Assert.IsInstanceOfType(actual, typeof(HumanBlackjackPlayerStrategy));
        }
    }
}
