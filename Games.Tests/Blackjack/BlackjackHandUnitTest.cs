﻿using Games.Framework.Blackjack.Cards;
using Games.Framework.Cards;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Games.Tests.Blackjack
{
    [TestClass]
    public class BlackjackHandUnitTest
    {
        #region IsBlackjack

        [TestMethod]
        public void TestIsBlackjackWithBlackjackHand()
        {
            // Arrange
            var target = new BlackjackHand();
            target.AddCard(new AceCard(PlayingCard.CardSuit.Spades).Turn());
            target.AddCard(new FaceCard(PlayingCard.CardSuit.Spades, 11).Turn());

            // Act
            var actual = target.IsBlackjack();

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestIsNotBlackjackWithValidBlackjackHandWithUnturnedCard()
        {
            // Arrange
            var target = new BlackjackHand();
            target.AddCard(new AceCard(PlayingCard.CardSuit.Spades));
            target.AddCard(new FaceCard(PlayingCard.CardSuit.Spades, 11).Turn());

            // Act
            var actual = target.IsBlackjack();

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsNotBlackjackWithInvalidBlackjackHandWithCard10()
        {
            // Arrange
            var target = new BlackjackHand();
            target.AddCard(new AceCard(PlayingCard.CardSuit.Spades).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 10).Turn());

            // Act
            var actual = target.IsBlackjack();

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsNotBlackjackWithInvalidBlackjackHandWithMultipleCards()
        {
            // Arrange
            var target = new BlackjackHand();
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 10));
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 5));
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 6));

            // Act
            var actual = target.IsBlackjack();

            // Assert
            Assert.IsFalse(actual);
        }

        #endregion

        #region GetValue

        [TestMethod]
        public void TestHandValueWithEmptyHand()
        {
            // Arrange
            var target = new BlackjackHand();

            // Act
            var actual = target.GetValue();

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void TestHandValueWithSoftHand()
        {
            // Arrange
            var target = new BlackjackHand();
            target.AddCard(new AceCard(PlayingCard.CardSuit.Spades).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 10).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 2).Turn());

            // Act
            var actual = target.GetValue();

            // Assert
            Assert.AreEqual(13, actual);
        }

        [TestMethod]
        public void TestHandValueWithHardHand()
        {
            // Arrange
            var target = new BlackjackHand();
            target.AddCard(new AceCard(PlayingCard.CardSuit.Spades).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 8).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 2).Turn());

            // Act
            var actual = target.GetValue();

            // Assert
            Assert.AreEqual(21, actual);
        }

        [TestMethod]
        public void TestHandValueWithBustedHand()
        {
            // Arrange
            var target = new BlackjackHand();
            target.AddCard(new FaceCard(PlayingCard.CardSuit.Spades, 11).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 8).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 4).Turn());

            // Act
            var actual = target.GetValue();

            // Assert
            Assert.AreEqual(22, actual);
        }

        [TestMethod]
        public void TestHandValueWithUnturnedHand()
        {
            // Arrange
            var target = new BlackjackHand();
            target.AddCard(new FaceCard(PlayingCard.CardSuit.Spades, 11).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 8).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 4));

            // Act
            var actual = target.GetValue();

            // Assert
            Assert.AreEqual(18, actual);
        }

        #endregion

        #region IsBusted

        [TestMethod]
        public void TestIsBustedWithValidHand()
        {
            // Arrange
            var target = new BlackjackHand();
            target.AddCard(new AceCard(PlayingCard.CardSuit.Spades));
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 8));
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 2));

            // Act
            var actual = target.IsBusted();

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsBustedWithBustedHand()
        {
            // Arrange
            var target = new BlackjackHand();
            target.AddCard(new FaceCard(PlayingCard.CardSuit.Spades, 11).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 8).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 4).Turn());

            // Act
            var actual = target.IsBusted();

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void TestIsNotBustedWithBustedHandWithUnturnedCard()
        {
            // Arrange
            var target = new BlackjackHand();
            target.AddCard(new FaceCard(PlayingCard.CardSuit.Spades, 11).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 8).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 4));

            // Act
            var actual = target.IsBusted();

            // Assert
            Assert.IsFalse(actual);
        }

        #endregion

        #region IsBelow

        [TestMethod]
        public void TestIsBelowIsFalseOnBustedHand()
        {
            // Arrange
            var target = new BlackjackHand();
            target.AddCard(new FaceCard(PlayingCard.CardSuit.Spades, 11).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 8).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 4).Turn());

            // Act
            var actual = target.IsBelow();

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIsBelowIsTrueOnRegularHand()
        {
            // Arrange
            var target = new BlackjackHand();
            target.AddCard(new FaceCard(PlayingCard.CardSuit.Spades, 11).Turn());
            target.AddCard(new NumericCard(PlayingCard.CardSuit.Spades, 8).Turn());

            // Act
            var actual = target.IsBelow();

            // Assert
            Assert.IsTrue(actual);
        }

        #endregion

    }
}
