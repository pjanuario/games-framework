﻿using Games.Framework.Cards;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Games.Tests.Cards
{
    [TestClass]
    public class FaceCardUnitTest
    {
        [TestMethod]
        public void TestIfIsNotAce()
        {
            // Arrange
            var card = new FaceCard(PlayingCard.CardSuit.Spades, 11);

            // Act
            var actual = card.IsAce();

            // Assert
            Assert.AreEqual(false, actual);
        }

        [TestMethod]
        public void TestIfIsFace()
        {
            // Arrange
            var card = new FaceCard(PlayingCard.CardSuit.Spades, 11);

            // Act
            var actual = card.IsFace();

            // Assert
            Assert.AreEqual(true, actual);
        }

        [TestMethod]
        public void TestIfValueIsParameterizedCorrectly()
        {
            // Arrange
            var card = new FaceCard(PlayingCard.CardSuit.Spades, 11);

            // Act
            var actual = card.Value;

            // Assert
            Assert.AreEqual(11, actual);
        }

        [TestMethod]
        public void TestIfCardSuiteIsParameterizedCorrectly()
        {
            // Arrange
            var card = new FaceCard(PlayingCard.CardSuit.Spades, 11);

            // Act
            var actual = card.Suit;

            // Assert
            Assert.AreEqual(PlayingCard.CardSuit.Spades, actual);
        }
    }
}
