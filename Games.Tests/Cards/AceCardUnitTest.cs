﻿using Games.Framework.Cards;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Games.Tests.Cards
{
    [TestClass]
    public class AceCardUnitTest
    {
        [TestMethod]
        public void TestIfIsAce()
        {
            // Arrange
            var card = new AceCard(PlayingCard.CardSuit.Spades);

            // Act
            var actual = card.IsAce();

            // Assert
            Assert.AreEqual(true, actual);
        }

        [TestMethod]
        public void TestIfIsNotFace()
        {
            // Arrange
            var card = new AceCard(PlayingCard.CardSuit.Spades);

            // Act
            var actual = card.IsFace();

            // Assert
            Assert.AreEqual(false, actual);
        }

        [TestMethod]
        public void TestIfDefaultValueEqualToOne()
        {
            // Arrange
            var card = new AceCard(PlayingCard.CardSuit.Spades);

            // Act
            var actual = card.Value;

            // Assert
            Assert.AreEqual(1, actual);
        }

        [TestMethod]
        public void TestIfCardSuiteIsParameterizedCorrectly()
        {
            // Arrange
            var card = new AceCard(PlayingCard.CardSuit.Spades);

            // Act
            var actual = card.Suit;

            // Assert
            Assert.AreEqual(PlayingCard.CardSuit.Spades, actual);
        }
    }
}
