﻿using Games.Framework.Cards;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Games.Tests.Cards
{
    [TestClass]
    public class PlayingCardUnitTest
    {
        [TestMethod]
        public void TestIfIsNotTurnedByDefault()
        {
            // Arrange
            PlayingCard card = new AceCard(PlayingCard.CardSuit.Spades);

            // Act
            var actual = card.IsTurned;

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void TestIfIsTurnedAfterTurn()
        {
            // Arrange
            PlayingCard card = new AceCard(PlayingCard.CardSuit.Spades);
            card.Turn();

            // Act
            var actual = card.IsTurned;

            // Assert
            Assert.IsTrue(actual);
        }
    }
}
