﻿using System.Collections.Generic;
using Games.Framework.Cards.Algorithm;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Games.Tests.Cards.Algorithm
{
    [TestClass]
    public class SimpleShuffleAlgorithmUnitTest
    {
        [TestMethod]
        public void TestSuffleWithGreatThan50PercentOfChangeOnList()
        {
            // Arrange
            IShuffleAlgorithm<int> target = new SimpleShuffleAlgorithm<int>();
            var list = new List<int>();
            const int listSize = 52;
            for (int i = 0; i < listSize; ++i)
                list.Add(i);

            var counterUnexchangedCards = 0;
            const int minChangeRate = 5;

            // Act
            target.Shuffle(list);

            // Assert
            for (int i = 0; i < listSize; ++i)
                if (i == list[i])
                    ++counterUnexchangedCards;

            Assert.IsTrue(counterUnexchangedCards <= minChangeRate, 
                "The number of exchanged cards should >= {0} and was excanged {1}", 
                listSize - minChangeRate, listSize - counterUnexchangedCards);
        }
    }
}
