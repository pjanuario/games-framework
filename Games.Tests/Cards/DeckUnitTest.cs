﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Games.Framework.Cards;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Games.Tests.Cards
{
    [TestClass]
    public class DeckUnitTest
    {
        #region Cards Total

        [TestMethod]
        public void TestIfDeckHave52Cards()
        {
            // Arrange
            IDeck target = new Deck();

            // Act
            var actual = target.TotalCardsLeft;

            //Assert
            Assert.AreEqual(52, actual);
        }

        [TestMethod]
        public void TestIfDeckCardsCollectionIsWith52Cards()
        {
            // Arrange
            IDeck target = new Deck();

            // Act
            var actual = target.Cards.Count();

            //Assert
            Assert.AreEqual(52, actual);
        }

        #endregion

        #region Test Deck Cards Collection

        [TestMethod]
        public void TestIfDeckCardsCollectionIsImmutable()
        {
            // Arrange
            var target = new Deck();

            // Act
            var actual = target.Cards;

            // Assert
            Assert.IsInstanceOfType(actual, typeof(ReadOnlyCollection<PlayingCard>));
        }

        [TestMethod]
        public void TestIfDeckCardsCollectionHave13CardsPerSuite()
        {
            // Arrange
            IDeck target = new Deck();

            // Act
            var actual = target.Cards.GroupBy(c => c.Suit);

            //Assert
            foreach (var suiteCollection in actual)
            {
                Assert.AreEqual(13, suiteCollection.Count(), "The suite {0} doesn't have 13 cards.", suiteCollection.Key);
            }
        }

        [TestMethod]
        public void TestIfDeckCardsCollectionHave91PointsPerSuite()
        {
            // Arrange
            IDeck target = new Deck();

            // Act
            var actual = target.Cards.GroupBy(c => c.Suit);

            //Assert
            foreach (var suiteCollection in actual)
            {
                Assert.AreEqual(91, suiteCollection.Sum(c => c.Value), "The suite {0} doesn't have 91 points.", suiteCollection.Key);
            }
        }

        [TestMethod]
        public void TestIfDeckCardsCollectionDoesnotHaveRepeatedCardsPerSuite()
        {
            // Arrange
            IDeck target = new Deck();

            // Act
            var actual = target.Cards.Distinct().Count();

            //Assert
            Assert.AreEqual(52, actual);
        }

        #endregion

        #region Test NextCard

        [TestMethod]
        public void TestNextCardIsNotNull()
        {
            // Arrange
            IDeck target = new Deck();

            // Act
            var actual = target.NextCard();

            // Assert
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void TestNextCardDoesNotReturnSameCardTwice()
        {
            // Arrange
            IDeck target = new Deck();
            var actual = new List<PlayingCard>(52);

            // Act
            for (int i = 0; i < 52; i++)
                actual.Add(target.NextCard());

            // Assert
            Assert.AreEqual(52, actual.Distinct().Count());
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void TestNextCardThrowsApplicationExceptionWhenAchieveEndOfDeck()
        {
            // Arrange
            IDeck target = new Deck();
            for (int i = 0; i < 52; i++)
                target.NextCard();

            // Act
            target.NextCard();

            // Assert
            // Expect exception
        }

        #endregion

        #region Test Suffle

        [TestMethod]
        public void TestNextCardAfterDifferentSuffles()
        {
            // Arrange
            IDeck target = new Deck();
            target.Suffle();
            var notExpected = target.NextCard();

            // Act
            target.Suffle();

            // Assert
            Assert.AreNotEqual(notExpected, target.NextCard());
        }

        #endregion

        #region Test Total Cards Left

        [TestMethod]
        public void TestTotalCardsLeftAfterSuffle()
        {
            // Arrange
            IDeck target = new Deck();
            target.Suffle();

            // Act
            var actual = target.TotalCardsLeft;

            // Assert
            Assert.AreEqual(52, actual);
        }

        [TestMethod]
        public void TestTotalCardsLeftAfterSuffleAndRemoveOneCard()
        {
            // Arrange
            IDeck target = new Deck();
            target.Suffle();
            target.NextCard();

            // Act
            var actual = target.TotalCardsLeft;

            // Assert
            Assert.AreEqual(51, actual);
        }

        #endregion
    }
}
