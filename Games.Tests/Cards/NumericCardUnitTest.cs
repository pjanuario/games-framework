﻿using Games.Framework.Cards;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Games.Tests.Cards
{
    [TestClass]
    public class NumericCardUnitTest
    {
        [TestMethod]
        public void TestIfIsNotAce()
        {
            // Arrange
            var card = new NumericCard(PlayingCard.CardSuit.Spades, 2);

            // Act
            var actual = card.IsAce();

            // Assert
            Assert.AreEqual(false, actual);
        }

        [TestMethod]
        public void TestIfIsNotFace()
        {
            // Arrange
            var card = new NumericCard(PlayingCard.CardSuit.Spades, 2);

            // Act
            var actual = card.IsFace();

            // Assert
            Assert.AreEqual(false, actual);
        }

        [TestMethod]
        public void TestIfValueIsParameterizedCorrectly()
        {
            // Arrange
            var card = new NumericCard(PlayingCard.CardSuit.Spades, 2);

            // Act
            var actual = card.Value;

            // Assert
            Assert.AreEqual(2, actual);
        }

        [TestMethod]
        public void TestIfCardSuiteIsParameterizedCorrectly()
        {
            // Arrange
            var card = new NumericCard(PlayingCard.CardSuit.Spades, 2);

            // Act
            var actual = card.Suit;

            // Assert
            Assert.AreEqual(PlayingCard.CardSuit.Spades, actual);
        }
    }
}
