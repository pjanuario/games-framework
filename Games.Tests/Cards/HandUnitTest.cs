﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Games.Framework.Cards;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Games.Tests.Cards
{
    [TestClass]
    public class HandUnitTest
    {
        internal class SimpleConcreteHand : Hand
        {
            public override int GetValue()
            {
                throw new System.NotImplementedException();
            }
        }

        internal virtual Hand CreateHand()
        {
            return new SimpleConcreteHand();
        }

        #region Add Card

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestAddCardWithNullArgumentAndExpectArgumentNullException()
        {
            // Arrange
            Hand target = CreateHand();

            // Act
            target.AddCard(null);

            // Assert
            // Expect exception
        }

        [TestMethod]
        public void TestAddCard()
        {
            // Arrange
            Hand target = CreateHand();
            var privateObj = new PrivateObject(target, new PrivateType(typeof(Hand)));

            PlayingCard card = new AceCard(PlayingCard.CardSuit.Spades);

            // Act
            target.AddCard(card);

            // Assert
            var privateCards = privateObj.GetField("cards");
            var cards = privateCards as List<PlayingCard>;
            Assert.IsInstanceOfType(privateCards, typeof(List<PlayingCard>));
            Assert.IsNotNull(cards);
            Assert.AreEqual(1, cards.Count);
            Assert.AreEqual(card, cards[0]);
        }

        #endregion

        #region Cards tests

        [TestMethod]
        public void TestCardsPropertyAsReadOnly()
        {
            // Arrange
            Hand target = CreateHand();
            var privateObj = new PrivateObject(target);

            // Act
            var privateCards = privateObj.GetProperty("Cards");

            // Assert
            Assert.IsInstanceOfType(privateCards, typeof(ReadOnlyCollection<PlayingCard>));
        }
        #endregion

        #region NumberOfCards

        [TestMethod]
        public void TestNumberOfCardsOnEmptyHand()
        {
            // Arrange
            Hand target = CreateHand();

            // Act
            var actual = target.NumberOfCards;

            // Assert
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void TestNumberOfCardsAddOneCard()
        {
            // Arrange
            Hand target = CreateHand();
            PlayingCard card = new AceCard(PlayingCard.CardSuit.Spades);
            target.AddCard(card);

            // Act
            var actual = target.NumberOfCards;

            // Assert
            Assert.AreEqual(1, actual);
        }

        #endregion
    }
}
