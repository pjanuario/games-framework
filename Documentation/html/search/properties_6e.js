var searchData=
[
  ['name',['Name',['../class_games_1_1_framework_1_1_game.html#aa6d04e66beebd7cef9336d550c0d60b9',1,'Games.Framework.Game.Name()'],['../class_games_1_1_framework_1_1_player_1_1_game_player.html#ab148e2377488e3baade656b897fb8a3f',1,'Games.Framework.Player.GamePlayer.Name()'],['../interface_games_1_1_framework_1_1_player_1_1_i_game_player.html#a092832bb05604d1dc5d76d80d78f338a',1,'Games.Framework.Player.IGamePlayer.Name()']]],
  ['numberofcards',['NumberOfCards',['../class_games_1_1_framework_1_1_cards_1_1_hand.html#a61c0b31b2f5ad3a1345d1627ce2bb01c',1,'Games.Framework.Cards.Hand.NumberOfCards()'],['../interface_games_1_1_framework_1_1_cards_1_1_i_hand.html#ab00bc15f31ef068d290db4452534aed4',1,'Games.Framework.Cards.IHand.NumberOfCards()']]],
  ['numberofplayers',['NumberOfPlayers',['../class_games_1_1_framework_1_1_game.html#a659b3f18a0b206412490010b880c5239',1,'Games::Framework::Game']]]
];
