var searchData=
[
  ['showdeckcards',['ShowDeckCards',['../class_games_1_1_framework_1_1_blackjack_1_1_blackjack_game.html#a01966508e93f99cf4927e924fcaadeca',1,'Games::Framework::Blackjack::BlackjackGame']]],
  ['showdeckcardsevent',['ShowDeckCardsEvent',['../class_games_1_1_framework_1_1_blackjack_1_1_blackjack_game.html#a64b47f4828048d92636fcb541e573365',1,'Games::Framework::Blackjack::BlackjackGame']]],
  ['shuffle_3c_20t_20_3e',['Shuffle&lt; T &gt;',['../interface_games_1_1_framework_1_1_cards_1_1_algorithm_1_1_i_shuffle_algorithm_3_01_t_01_4.html#a4b2dae2d7e87490ceba2606a10bd05d2',1,'Games.Framework.Cards.Algorithm.IShuffleAlgorithm&lt; T &gt;.Shuffle&lt; T &gt;()'],['../class_games_1_1_framework_1_1_cards_1_1_algorithm_1_1_simple_shuffle_algorithm_3_01_t_01_4.html#a6765809cc3762ebf6b61e9b91c526b52',1,'Games.Framework.Cards.Algorithm.SimpleShuffleAlgorithm&lt; T &gt;.Shuffle&lt; T &gt;()']]],
  ['simpleshufflealgorithm_2ecs',['SimpleShuffleAlgorithm.cs',['../_simple_shuffle_algorithm_8cs.html',1,'']]],
  ['simpleshufflealgorithm_3c_20t_20_3e',['SimpleShuffleAlgorithm&lt; T &gt;',['../class_games_1_1_framework_1_1_cards_1_1_algorithm_1_1_simple_shuffle_algorithm_3_01_t_01_4.html',1,'Games::Framework::Cards::Algorithm']]],
  ['simpleshufflealgorithmunittest',['SimpleShuffleAlgorithmUnitTest',['../class_games_1_1_tests_1_1_cards_1_1_algorithm_1_1_simple_shuffle_algorithm_unit_test.html',1,'Games::Tests::Cards::Algorithm']]],
  ['simpleshufflealgorithmunittest_2ecs',['SimpleShuffleAlgorithmUnitTest.cs',['../_simple_shuffle_algorithm_unit_test_8cs.html',1,'']]],
  ['spades',['Spades',['../class_games_1_1_framework_1_1_cards_1_1_playing_card.html#ad0f60121dddf7be6b86a394cc9849ea6a8dac9a2d5a20ebbf46559d684bae0f46',1,'Games::Framework::Cards::PlayingCard']]],
  ['split',['Split',['../namespace_games_1_1_framework_1_1_blackjack_1_1_player.html#a1d29dfbf4a6ee2d7492723294017aa8ca8a9e64d86ed12ad40de129bc7f4683b2',1,'Games::Framework::Blackjack::Player']]],
  ['stand',['Stand',['../namespace_games_1_1_framework_1_1_blackjack_1_1_player.html#a1d29dfbf4a6ee2d7492723294017aa8caf5e531e3638cf8a722a9cd26831e108f',1,'Games::Framework::Blackjack::Player']]],
  ['start',['Start',['../class_games_1_1_framework_1_1_blackjack_1_1_blackjack_game.html#a78e5670f8c889bccc5f8115f7878f530',1,'Games::Framework::Blackjack::BlackjackGame']]],
  ['startnewgame',['StartNewGame',['../class_games_1_1_console_g_u_i_1_1_console_app_blackjack_game.html#a7982cf824ac23295068ef67ebaa0dc63',1,'Games::ConsoleGUI::ConsoleAppBlackjackGame']]],
  ['strategy',['Strategy',['../class_games_1_1_framework_1_1_blackjack_1_1_player_1_1_blackjack_player.html#a9bf3493313ec9a1ec5f0ad764ac69cad',1,'Games::Framework::Blackjack::Player::BlackjackPlayer']]],
  ['suffle',['Suffle',['../class_games_1_1_framework_1_1_cards_1_1_deck.html#a2c177f9bedc5bcc7258dc5b828ebbff2',1,'Games.Framework.Cards.Deck.Suffle()'],['../interface_games_1_1_framework_1_1_cards_1_1_i_deck.html#a6a7f6adb6d2899055bcf0a839ad489bc',1,'Games.Framework.Cards.IDeck.Suffle()']]],
  ['suit',['Suit',['../class_games_1_1_framework_1_1_cards_1_1_playing_card.html#adb79ffed35f0567db57b82c4be67f89c',1,'Games::Framework::Cards::PlayingCard']]],
  ['surrender',['Surrender',['../namespace_games_1_1_framework_1_1_blackjack_1_1_player.html#a1d29dfbf4a6ee2d7492723294017aa8ca92fa0b43cbcc2b7610cdb157d273c5fa',1,'Games::Framework::Blackjack::Player']]]
];
