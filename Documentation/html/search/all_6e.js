var searchData=
[
  ['name',['Name',['../class_games_1_1_framework_1_1_game.html#aa6d04e66beebd7cef9336d550c0d60b9',1,'Games.Framework.Game.Name()'],['../class_games_1_1_framework_1_1_player_1_1_game_player.html#ab148e2377488e3baade656b897fb8a3f',1,'Games.Framework.Player.GamePlayer.Name()'],['../interface_games_1_1_framework_1_1_player_1_1_i_game_player.html#a092832bb05604d1dc5d76d80d78f338a',1,'Games.Framework.Player.IGamePlayer.Name()']]],
  ['nextcard',['NextCard',['../class_games_1_1_framework_1_1_cards_1_1_deck.html#a8efcec8daa93b745c711d62c2ad95f63',1,'Games.Framework.Cards.Deck.NextCard()'],['../interface_games_1_1_framework_1_1_cards_1_1_i_deck.html#aa4a2d880e7981a0735b67e55ad46b337',1,'Games.Framework.Cards.IDeck.NextCard()']]],
  ['numberofcards',['NumberOfCards',['../class_games_1_1_framework_1_1_cards_1_1_hand.html#a61c0b31b2f5ad3a1345d1627ce2bb01c',1,'Games.Framework.Cards.Hand.NumberOfCards()'],['../interface_games_1_1_framework_1_1_cards_1_1_i_hand.html#ab00bc15f31ef068d290db4452534aed4',1,'Games.Framework.Cards.IHand.NumberOfCards()']]],
  ['numberofplayers',['NumberOfPlayers',['../class_games_1_1_framework_1_1_game.html#a659b3f18a0b206412490010b880c5239',1,'Games::Framework::Game']]],
  ['numericcard',['NumericCard',['../class_games_1_1_framework_1_1_cards_1_1_numeric_card.html',1,'Games::Framework::Cards']]],
  ['numericcard',['NumericCard',['../class_games_1_1_framework_1_1_cards_1_1_numeric_card.html#a3c79cfad2972ca4710a68aa6d89c35a7',1,'Games::Framework::Cards::NumericCard']]],
  ['numericcard_2ecs',['NumericCard.cs',['../_numeric_card_8cs.html',1,'']]],
  ['numericcardunittest',['NumericCardUnitTest',['../class_games_1_1_tests_1_1_cards_1_1_numeric_card_unit_test.html',1,'Games::Tests::Cards']]],
  ['numericcardunittest_2ecs',['NumericCardUnitTest.cs',['../_numeric_card_unit_test_8cs.html',1,'']]]
];
