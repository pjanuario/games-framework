var dir_26e7d9c80ab2606ed0580905ce028252 =
[
    [ "Blackjack", "dir_d6833843dd87d0fd569a2ea262c125c0.html", "dir_d6833843dd87d0fd569a2ea262c125c0" ],
    [ "Cards", "dir_5d23e0832784b13b363149b687cd7fe8.html", "dir_5d23e0832784b13b363149b687cd7fe8" ],
    [ "Player", "dir_3a4212cd3e6815c07dd657a9b13e3685.html", "dir_3a4212cd3e6815c07dd657a9b13e3685" ],
    [ "Properties", "dir_acf0f6fc7c71b3b2a72d03efda0501d8.html", "dir_acf0f6fc7c71b3b2a72d03efda0501d8" ],
    [ "Command.cs", "_command_8cs.html", [
      [ "Command", "class_games_1_1_framework_1_1_command.html", "class_games_1_1_framework_1_1_command" ]
    ] ],
    [ "Game.cs", "_game_8cs.html", [
      [ "Game", "class_games_1_1_framework_1_1_game.html", "class_games_1_1_framework_1_1_game" ]
    ] ],
    [ "ICommand.cs", "_i_command_8cs.html", [
      [ "ICommand", "interface_games_1_1_framework_1_1_i_command.html", "interface_games_1_1_framework_1_1_i_command" ]
    ] ]
];