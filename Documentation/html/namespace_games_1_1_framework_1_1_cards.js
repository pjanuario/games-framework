var namespace_games_1_1_framework_1_1_cards =
[
    [ "Algorithm", "namespace_games_1_1_framework_1_1_cards_1_1_algorithm.html", "namespace_games_1_1_framework_1_1_cards_1_1_algorithm" ],
    [ "AceCard", "class_games_1_1_framework_1_1_cards_1_1_ace_card.html", "class_games_1_1_framework_1_1_cards_1_1_ace_card" ],
    [ "Deck", "class_games_1_1_framework_1_1_cards_1_1_deck.html", "class_games_1_1_framework_1_1_cards_1_1_deck" ],
    [ "FaceCard", "class_games_1_1_framework_1_1_cards_1_1_face_card.html", "class_games_1_1_framework_1_1_cards_1_1_face_card" ],
    [ "Hand", "class_games_1_1_framework_1_1_cards_1_1_hand.html", "class_games_1_1_framework_1_1_cards_1_1_hand" ],
    [ "IDeck", "interface_games_1_1_framework_1_1_cards_1_1_i_deck.html", "interface_games_1_1_framework_1_1_cards_1_1_i_deck" ],
    [ "IHand", "interface_games_1_1_framework_1_1_cards_1_1_i_hand.html", "interface_games_1_1_framework_1_1_cards_1_1_i_hand" ],
    [ "NumericCard", "class_games_1_1_framework_1_1_cards_1_1_numeric_card.html", "class_games_1_1_framework_1_1_cards_1_1_numeric_card" ],
    [ "PlayingCard", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html", "class_games_1_1_framework_1_1_cards_1_1_playing_card" ]
];