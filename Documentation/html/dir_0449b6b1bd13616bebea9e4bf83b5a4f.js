var dir_0449b6b1bd13616bebea9e4bf83b5a4f =
[
    [ "Algorithm", "dir_91c0747f019c18f5aa43f88f0ed999bc.html", "dir_91c0747f019c18f5aa43f88f0ed999bc" ],
    [ "AceCardUnitTest.cs", "_ace_card_unit_test_8cs.html", [
      [ "AceCardUnitTest", "class_games_1_1_tests_1_1_cards_1_1_ace_card_unit_test.html", "class_games_1_1_tests_1_1_cards_1_1_ace_card_unit_test" ]
    ] ],
    [ "DeckUnitTest.cs", "_deck_unit_test_8cs.html", [
      [ "DeckUnitTest", "class_games_1_1_tests_1_1_cards_1_1_deck_unit_test.html", "class_games_1_1_tests_1_1_cards_1_1_deck_unit_test" ]
    ] ],
    [ "FaceCardUnitTest.cs", "_face_card_unit_test_8cs.html", [
      [ "FaceCardUnitTest", "class_games_1_1_tests_1_1_cards_1_1_face_card_unit_test.html", "class_games_1_1_tests_1_1_cards_1_1_face_card_unit_test" ]
    ] ],
    [ "HandUnitTest.cs", "_hand_unit_test_8cs.html", [
      [ "HandUnitTest", "class_games_1_1_tests_1_1_cards_1_1_hand_unit_test.html", "class_games_1_1_tests_1_1_cards_1_1_hand_unit_test" ]
    ] ],
    [ "NumericCardUnitTest.cs", "_numeric_card_unit_test_8cs.html", [
      [ "NumericCardUnitTest", "class_games_1_1_tests_1_1_cards_1_1_numeric_card_unit_test.html", "class_games_1_1_tests_1_1_cards_1_1_numeric_card_unit_test" ]
    ] ],
    [ "PlayingCardUnitTest.cs", "_playing_card_unit_test_8cs.html", [
      [ "PlayingCardUnitTest", "class_games_1_1_tests_1_1_cards_1_1_playing_card_unit_test.html", "class_games_1_1_tests_1_1_cards_1_1_playing_card_unit_test" ]
    ] ]
];