var hierarchy =
[
    [ "Games.Tests.Cards.AceCardUnitTest", "class_games_1_1_tests_1_1_cards_1_1_ace_card_unit_test.html", null ],
    [ "Games.ConsoleGUI.BlackjackGameRunner", "class_games_1_1_console_g_u_i_1_1_blackjack_game_runner.html", null ],
    [ "Games.Tests.Blackjack.BlackjackHandUnitTest", "class_games_1_1_tests_1_1_blackjack_1_1_blackjack_hand_unit_test.html", null ],
    [ "Games.Framework.Blackjack.Player.BlackjackPlayerStrategyFactory", "class_games_1_1_framework_1_1_blackjack_1_1_player_1_1_blackjack_player_strategy_factory.html", null ],
    [ "Games.Tests.Blackjack.Player.BlackjackPlayerStrategyFactoryUnitTest", "class_games_1_1_tests_1_1_blackjack_1_1_player_1_1_blackjack_player_strategy_factory_unit_test.html", null ],
    [ "Games.Tests.Blackjack.Player.BlackjackPlayerUnitTest", "class_games_1_1_tests_1_1_blackjack_1_1_player_1_1_blackjack_player_unit_test.html", null ],
    [ "Games.Tests.Blackjack.Player.DealerBlackjackPlayerStrategyUnitTest", "class_games_1_1_tests_1_1_blackjack_1_1_player_1_1_dealer_blackjack_player_strategy_unit_test.html", null ],
    [ "Games.Tests.Cards.DeckUnitTest", "class_games_1_1_tests_1_1_cards_1_1_deck_unit_test.html", null ],
    [ "Games.Tests.Cards.FaceCardUnitTest", "class_games_1_1_tests_1_1_cards_1_1_face_card_unit_test.html", null ],
    [ "Games.Framework.Game", "class_games_1_1_framework_1_1_game.html", [
      [ "Games.Framework.Blackjack.BlackjackGame", "class_games_1_1_framework_1_1_blackjack_1_1_blackjack_game.html", null ]
    ] ],
    [ "Games.Tests.Cards.HandUnitTest", "class_games_1_1_tests_1_1_cards_1_1_hand_unit_test.html", null ],
    [ "Games.Framework.Blackjack.IBlackjackPlayCommand", "interface_games_1_1_framework_1_1_blackjack_1_1_i_blackjack_play_command.html", [
      [ "Games.Framework.Blackjack.BlackjackPlayCommand", "class_games_1_1_framework_1_1_blackjack_1_1_blackjack_play_command.html", null ]
    ] ],
    [ "Games.Framework.Blackjack.IBlackjackPlayCommandReceiver", "interface_games_1_1_framework_1_1_blackjack_1_1_i_blackjack_play_command_receiver.html", [
      [ "Games.ConsoleGUI.ConsoleAppBlackjackGame", "class_games_1_1_console_g_u_i_1_1_console_app_blackjack_game.html", null ]
    ] ],
    [ "Games.Framework.Blackjack.Player.IBlackjackPlayerStrategy", "interface_games_1_1_framework_1_1_blackjack_1_1_player_1_1_i_blackjack_player_strategy.html", [
      [ "Games.Framework.Blackjack.Player.DealerBlackjackPlayerStrategy", "class_games_1_1_framework_1_1_blackjack_1_1_player_1_1_dealer_blackjack_player_strategy.html", null ],
      [ "Games.Framework.Blackjack.Player.HumanBlackjackPlayerStrategy", "class_games_1_1_framework_1_1_blackjack_1_1_player_1_1_human_blackjack_player_strategy.html", null ]
    ] ],
    [ "Games.Framework.ICommand", "interface_games_1_1_framework_1_1_i_command.html", [
      [ "Games.Framework.Command", "class_games_1_1_framework_1_1_command.html", [
        [ "Games.Framework.Blackjack.BlackjackPlayCommand", "class_games_1_1_framework_1_1_blackjack_1_1_blackjack_play_command.html", null ]
      ] ]
    ] ],
    [ "Games.Framework.Cards.IDeck", "interface_games_1_1_framework_1_1_cards_1_1_i_deck.html", [
      [ "Games.Framework.Cards.Deck", "class_games_1_1_framework_1_1_cards_1_1_deck.html", null ]
    ] ],
    [ "IEquatable< PlayingCard >", null, [
      [ "Games.Framework.Cards.PlayingCard", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html", [
        [ "Games.Framework.Cards.AceCard", "class_games_1_1_framework_1_1_cards_1_1_ace_card.html", null ],
        [ "Games.Framework.Cards.FaceCard", "class_games_1_1_framework_1_1_cards_1_1_face_card.html", null ],
        [ "Games.Framework.Cards.NumericCard", "class_games_1_1_framework_1_1_cards_1_1_numeric_card.html", null ]
      ] ]
    ] ],
    [ "Games.Framework.Player.IGamePlayer", "interface_games_1_1_framework_1_1_player_1_1_i_game_player.html", [
      [ "Games.Framework.Player.GamePlayer", "class_games_1_1_framework_1_1_player_1_1_game_player.html", [
        [ "Games.Framework.Blackjack.Player.BlackjackPlayer", "class_games_1_1_framework_1_1_blackjack_1_1_player_1_1_blackjack_player.html", null ]
      ] ]
    ] ],
    [ "Games.Framework.Cards.IHand", "interface_games_1_1_framework_1_1_cards_1_1_i_hand.html", [
      [ "Games.Framework.Cards.Hand", "class_games_1_1_framework_1_1_cards_1_1_hand.html", [
        [ "Games.Framework.Blackjack.Cards.BlackjackHand", "class_games_1_1_framework_1_1_blackjack_1_1_cards_1_1_blackjack_hand.html", null ]
      ] ]
    ] ],
    [ "Games.Framework.Cards.Algorithm.IShuffleAlgorithm< T >", "interface_games_1_1_framework_1_1_cards_1_1_algorithm_1_1_i_shuffle_algorithm_3_01_t_01_4.html", [
      [ "Games.Framework.Cards.Algorithm.SimpleShuffleAlgorithm< T >", "class_games_1_1_framework_1_1_cards_1_1_algorithm_1_1_simple_shuffle_algorithm_3_01_t_01_4.html", null ]
    ] ],
    [ "Games.Tests.Cards.NumericCardUnitTest", "class_games_1_1_tests_1_1_cards_1_1_numeric_card_unit_test.html", null ],
    [ "Games.Tests.Cards.PlayingCardUnitTest", "class_games_1_1_tests_1_1_cards_1_1_playing_card_unit_test.html", null ],
    [ "Games.Tests.Cards.Algorithm.SimpleShuffleAlgorithmUnitTest", "class_games_1_1_tests_1_1_cards_1_1_algorithm_1_1_simple_shuffle_algorithm_unit_test.html", null ]
];