var class_games_1_1_tests_1_1_cards_1_1_deck_unit_test =
[
    [ "TestIfDeckCardsCollectionDoesnotHaveRepeatedCardsPerSuite", "class_games_1_1_tests_1_1_cards_1_1_deck_unit_test.html#ab1aef76162426538b1c04c37fc5e1eb5", null ],
    [ "TestIfDeckCardsCollectionHave13CardsPerSuite", "class_games_1_1_tests_1_1_cards_1_1_deck_unit_test.html#a2bfd18a37f1ec0f99660a7b94b394ef7", null ],
    [ "TestIfDeckCardsCollectionHave91PointsPerSuite", "class_games_1_1_tests_1_1_cards_1_1_deck_unit_test.html#ad7aa49bb31b730db407131cad7152811", null ],
    [ "TestIfDeckCardsCollectionIsImmutable", "class_games_1_1_tests_1_1_cards_1_1_deck_unit_test.html#ac59e0234fc791a49de791655683f4655", null ],
    [ "TestIfDeckCardsCollectionIsWith52Cards", "class_games_1_1_tests_1_1_cards_1_1_deck_unit_test.html#a650ca41a8e3a3a05e9c639610bd595f2", null ],
    [ "TestIfDeckHave52Cards", "class_games_1_1_tests_1_1_cards_1_1_deck_unit_test.html#ac0066e323a6b6e8a16e8611567bbb125", null ],
    [ "TestNextCardAfterDifferentSuffles", "class_games_1_1_tests_1_1_cards_1_1_deck_unit_test.html#a47a4b0ce235d33fad4fa475c14261144", null ],
    [ "TestNextCardDoesNotReturnSameCardTwice", "class_games_1_1_tests_1_1_cards_1_1_deck_unit_test.html#a26a1653d8b6d849969abb0b5f0efac2d", null ],
    [ "TestNextCardIsNotNull", "class_games_1_1_tests_1_1_cards_1_1_deck_unit_test.html#af6ba2997ccb60d52c1e7b39019555a9f", null ],
    [ "TestNextCardThrowsApplicationExceptionWhenAchieveEndOfDeck", "class_games_1_1_tests_1_1_cards_1_1_deck_unit_test.html#a97767e9c23dc560918107a4f1caf8995", null ],
    [ "TestTotalCardsLeftAfterSuffle", "class_games_1_1_tests_1_1_cards_1_1_deck_unit_test.html#a336da5daeebd9e1bce257a9c6a1a17c3", null ],
    [ "TestTotalCardsLeftAfterSuffleAndRemoveOneCard", "class_games_1_1_tests_1_1_cards_1_1_deck_unit_test.html#a73159ab92e0e17238fc03b2edaa04434", null ]
];