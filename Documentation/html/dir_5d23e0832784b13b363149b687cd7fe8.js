var dir_5d23e0832784b13b363149b687cd7fe8 =
[
    [ "Algorithm", "dir_d0cbb72f93de943d9ccee45b32bbc9ed.html", "dir_d0cbb72f93de943d9ccee45b32bbc9ed" ],
    [ "AceCard.cs", "_ace_card_8cs.html", [
      [ "AceCard", "class_games_1_1_framework_1_1_cards_1_1_ace_card.html", "class_games_1_1_framework_1_1_cards_1_1_ace_card" ]
    ] ],
    [ "Deck.cs", "_deck_8cs.html", [
      [ "Deck", "class_games_1_1_framework_1_1_cards_1_1_deck.html", "class_games_1_1_framework_1_1_cards_1_1_deck" ]
    ] ],
    [ "FaceCard.cs", "_face_card_8cs.html", [
      [ "FaceCard", "class_games_1_1_framework_1_1_cards_1_1_face_card.html", "class_games_1_1_framework_1_1_cards_1_1_face_card" ]
    ] ],
    [ "Hand.cs", "_hand_8cs.html", [
      [ "Hand", "class_games_1_1_framework_1_1_cards_1_1_hand.html", "class_games_1_1_framework_1_1_cards_1_1_hand" ]
    ] ],
    [ "IDeck.cs", "_i_deck_8cs.html", [
      [ "IDeck", "interface_games_1_1_framework_1_1_cards_1_1_i_deck.html", "interface_games_1_1_framework_1_1_cards_1_1_i_deck" ]
    ] ],
    [ "IHand.cs", "_i_hand_8cs.html", [
      [ "IHand", "interface_games_1_1_framework_1_1_cards_1_1_i_hand.html", "interface_games_1_1_framework_1_1_cards_1_1_i_hand" ]
    ] ],
    [ "NumericCard.cs", "_numeric_card_8cs.html", [
      [ "NumericCard", "class_games_1_1_framework_1_1_cards_1_1_numeric_card.html", "class_games_1_1_framework_1_1_cards_1_1_numeric_card" ]
    ] ],
    [ "PlayingCard.cs", "_playing_card_8cs.html", [
      [ "PlayingCard", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html", "class_games_1_1_framework_1_1_cards_1_1_playing_card" ]
    ] ]
];