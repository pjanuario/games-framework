var class_games_1_1_framework_1_1_cards_1_1_hand =
[
    [ "Hand", "class_games_1_1_framework_1_1_cards_1_1_hand.html#a8f2c94891f6e33f34d8d91bd657e20a3", null ],
    [ "AddCard", "class_games_1_1_framework_1_1_cards_1_1_hand.html#ac14ed6c5512a511db65b0d71361cb307", null ],
    [ "GetValue", "class_games_1_1_framework_1_1_cards_1_1_hand.html#af0722853ca856e3202bffc0cbceb4b4f", null ],
    [ "Cards", "class_games_1_1_framework_1_1_cards_1_1_hand.html#a0bf113da51a2f6240e929084b3044f85", null ],
    [ "NumberOfCards", "class_games_1_1_framework_1_1_cards_1_1_hand.html#a61c0b31b2f5ad3a1345d1627ce2bb01c", null ]
];