var class_games_1_1_tests_1_1_cards_1_1_hand_unit_test =
[
    [ "TestAddCard", "class_games_1_1_tests_1_1_cards_1_1_hand_unit_test.html#acc5bf2909692f123cb866a6bc84acef4", null ],
    [ "TestAddCardWithNullArgumentAndExpectArgumentNullException", "class_games_1_1_tests_1_1_cards_1_1_hand_unit_test.html#a51bd5ba8f0da958314bb60e8dcc88da5", null ],
    [ "TestCardsPropertyAsReadOnly", "class_games_1_1_tests_1_1_cards_1_1_hand_unit_test.html#aae3eb1f337b67ec5310ee126392f3c49", null ],
    [ "TestNumberOfCardsAddOneCard", "class_games_1_1_tests_1_1_cards_1_1_hand_unit_test.html#ad169f4fb3ea4fd93c84e5183f67043ac", null ],
    [ "TestNumberOfCardsOnEmptyHand", "class_games_1_1_tests_1_1_cards_1_1_hand_unit_test.html#a44d4c8793e41c56b962f18282b519aa7", null ]
];