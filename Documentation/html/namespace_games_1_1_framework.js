var namespace_games_1_1_framework =
[
    [ "Blackjack", "namespace_games_1_1_framework_1_1_blackjack.html", "namespace_games_1_1_framework_1_1_blackjack" ],
    [ "Cards", "namespace_games_1_1_framework_1_1_cards.html", "namespace_games_1_1_framework_1_1_cards" ],
    [ "Player", "namespace_games_1_1_framework_1_1_player.html", "namespace_games_1_1_framework_1_1_player" ],
    [ "Command", "class_games_1_1_framework_1_1_command.html", "class_games_1_1_framework_1_1_command" ],
    [ "Game", "class_games_1_1_framework_1_1_game.html", "class_games_1_1_framework_1_1_game" ],
    [ "ICommand", "interface_games_1_1_framework_1_1_i_command.html", "interface_games_1_1_framework_1_1_i_command" ]
];