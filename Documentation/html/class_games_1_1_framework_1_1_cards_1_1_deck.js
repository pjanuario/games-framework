var class_games_1_1_framework_1_1_cards_1_1_deck =
[
    [ "Deck", "class_games_1_1_framework_1_1_cards_1_1_deck.html#ac7e0f00193c027f7e20be2f0e446969c", null ],
    [ "Deck", "class_games_1_1_framework_1_1_cards_1_1_deck.html#ad91ac08c1b75f8d0a0cbd57b39b8e4be", null ],
    [ "NextCard", "class_games_1_1_framework_1_1_cards_1_1_deck.html#a8efcec8daa93b745c711d62c2ad95f63", null ],
    [ "Suffle", "class_games_1_1_framework_1_1_cards_1_1_deck.html#a2c177f9bedc5bcc7258dc5b828ebbff2", null ],
    [ "Cards", "class_games_1_1_framework_1_1_cards_1_1_deck.html#ac81b0b475e08fb1a7276c671bd06d353", null ],
    [ "TotalCardsLeft", "class_games_1_1_framework_1_1_cards_1_1_deck.html#aa84dd3acfc363b947b8023133ad15166", null ]
];