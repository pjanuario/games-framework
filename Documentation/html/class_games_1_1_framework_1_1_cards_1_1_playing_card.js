var class_games_1_1_framework_1_1_cards_1_1_playing_card =
[
    [ "CardSuit", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html#ad0f60121dddf7be6b86a394cc9849ea6", [
      [ "Hearts", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html#ad0f60121dddf7be6b86a394cc9849ea6a6af4bf214d6cf054f9528da5b6054817", null ],
      [ "Spades", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html#ad0f60121dddf7be6b86a394cc9849ea6a8dac9a2d5a20ebbf46559d684bae0f46", null ],
      [ "Diamonds", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html#ad0f60121dddf7be6b86a394cc9849ea6a74d868fafa8c57d04565f672807e7ef7", null ],
      [ "Clubs", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html#ad0f60121dddf7be6b86a394cc9849ea6abc56e1ae1e30e7123ffc5030f108f2a0", null ]
    ] ],
    [ "PlayingCard", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html#ace4a52d634631783d1360898c055497c", null ],
    [ "Equals", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html#aa53dc306d3d621bea8e327aed972aed3", null ],
    [ "Equals", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html#aa833f7ea7f9297064b6c0a5671edca0d", null ],
    [ "GetHashCode", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html#a83fd9b7b645025ad3ef2b723b7d9d5dd", null ],
    [ "IsAce", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html#a41dee26ec73296417cd4e64e2379f3ce", null ],
    [ "IsFace", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html#aa47ccb94ee670f05d63efe169cdab9fc", null ],
    [ "Turn", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html#aafe3a3eaf47a635576bb035e54f985e3", null ],
    [ "IsTurned", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html#a7192e7f764cd34a0f207e3e8bde04251", null ],
    [ "Suit", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html#adb79ffed35f0567db57b82c4be67f89c", null ],
    [ "Value", "class_games_1_1_framework_1_1_cards_1_1_playing_card.html#a5a3eb27a9f7ae5d3542c20a17aed294d", null ]
];