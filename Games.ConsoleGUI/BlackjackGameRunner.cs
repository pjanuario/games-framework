﻿using System;

namespace Games.ConsoleGUI
{
    /// <summary>
    /// Represents an entry point for the Game.
    /// </summary>
    class BlackjackGameRunner
    {
        [STAThread]
        static void Main()
        {
            new ConsoleAppBlackjackGame().StartNewGame();
        }
    }
}
