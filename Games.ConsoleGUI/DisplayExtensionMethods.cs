﻿using System;
using System.Collections.Generic;
using Games.Framework.Cards;
using Games.Framework.Player;

namespace Games.ConsoleGUI
{
    /// <summary>
    /// This class act as decorator for model classes extending the base functionality with display methods.
    /// </summary>
    static class DisplayExtensionMethods
    {
        private static readonly string[] CardDisplayText = new[] { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };
        private static readonly string[] SuiteDisplaySymbol = new [] { "♥", "♠", "♦", "♣"};

        /// <summary>
        /// Displays the specified card.
        /// </summary>
        /// <param name="card">The card.</param>
        /// <param name="displayMethod">The display method.</param>
        public static void Display(this PlayingCard card, Action<string> displayMethod)
        {
            displayMethod(card.IsTurned ? ToString(card) : "?");
        }

        /// <summary>
        /// Displays the specified cards.
        /// </summary>
        /// <param name="cards">The cards.</param>
        /// <param name="displayMethod">The display method.</param>
        public static void Display(this IEnumerable<PlayingCard> cards, Action<string> displayMethod)
        {
            int i = 0;
            foreach (PlayingCard card in cards)
            {
                ++i;
                displayMethod(ToString(card));

                if (i % 13 == 0)
                    displayMethod("\n");
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <param name="card">The card.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        private static string ToString(PlayingCard card)
        {
            return CardDisplayText[card.Value - 1] + SuiteDisplaySymbol[(int)card.Suit];
        }

        /// <summary>
        /// Displays the specified hand.
        /// </summary>
        /// <param name="hand">The hand.</param>
        /// <param name="displayMethod">The display method.</param>
        public static void Display(this IHand hand, Action<string> displayMethod)
        {
            displayMethod("Hand( ");

            foreach (var card in hand.Cards)
            {
                card.Display(displayMethod);
                displayMethod(" ");
            }
            displayMethod(string.Format(" Value - {0})", hand.GetValue()));

        }

        /// <summary>
        /// Displays the specified player.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <param name="displayMethod">The display method.</param>
        public static void Display(this IGamePlayer player, Action<string> displayMethod)
        {
            displayMethod(player.Name + ": ");
            player.Hand.Display(displayMethod);
        }
    }
}