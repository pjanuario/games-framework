﻿using System;
using System.Collections.Generic;
using Games.Framework.Blackjack;
using Games.Framework.Blackjack.Player;
using Games.Framework.Cards;
using Games.Framework.Player;

namespace Games.ConsoleGUI
{
    /// <summary>
    /// Represents a Console application as graphic user interface.
    /// </summary>
    public class ConsoleAppBlackjackGame : IBlackjackPlayCommandReceiver
    {
        /// <summary>
        /// The blackjack game.
        /// </summary>
        private readonly BlackjackGame game;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConsoleAppBlackjackGame"/> class.
        /// </summary>
        public ConsoleAppBlackjackGame()
        {
            game = new BlackjackGame(1, this);

            game.PlayerTurnEvent += OnPlayerTurn;
            game.PlayerHandChangedEvent += OnPlayerHandChanged;
            game.ShowDeckCardsEvent += OnShowCards;
            game.DeckShuffledEvent += OnDeckShuffled;
            game.PlayerResultEvent += OnPlayerGameResult;
        }

        /// <summary>
        /// Starts the new game.
        /// </summary>
        public void StartNewGame()
        {
            try
            {
                game.Init();

                string response;
                do
                {
                    Console.WriteLine("**** Start new Blackjack game **** ");
                    Console.WriteLine();

                    game.Start();

                    Console.WriteLine();
                    Console.WriteLine("**** End ****");
                    Console.WriteLine(); Console.WriteLine();

                    Console.WriteLine();
                    Console.WriteLine("Restart game?");
                    
                    response = Console.ReadLine().Trim().ToUpper();
                    
                    Console.WriteLine(); Console.WriteLine();

                } while (response == "Y" || response == "YES");
            }
            catch (Exception e)
            {
                Console.WriteLine("**** Exit unexpected error with message: '{0}' ****", e.Message);
                Console.WriteLine();
            }
            Console.WriteLine("Hit any key to exit...");
            Console.Read();
        }

        /// <summary>
        /// Called when [player hand changed].
        /// </summary>
        /// <param name="player">The player.</param>
        private void OnPlayerHandChanged(IGamePlayer player)
        {
            player.Display(Console.Write);
            Console.WriteLine();
            Console.WriteLine();
        }

        /// <summary>
        /// Called when [show cards].
        /// </summary>
        /// <param name="cards">The cards.</param>
        private void OnShowCards(IEnumerable<PlayingCard> cards)
        {
            cards.Display(Console.Write);
            Console.WriteLine();
        }

        /// <summary>
        /// Called when [player game result].
        /// </summary>
        /// <param name="player">The player.</param>
        /// <param name="result">The result.</param>
        private void OnPlayerGameResult(IGamePlayer player, BlackjackResult result)
        {
            player.Display(Console.Write);
            Console.WriteLine(" - {0}", result.ToString());
            Console.WriteLine();
        }

        /// <summary>
        /// Called when [deck shuffled].
        /// </summary>
        private void OnDeckShuffled()
        {
            Console.WriteLine("Shuffled...");
            Console.WriteLine();
        }

        /// <summary>
        /// Called when [player turn].
        /// </summary>
        /// <param name="player">The player.</param>
        private void OnPlayerTurn(IGamePlayer player)
        {
            Console.WriteLine();
            Console.WriteLine("Player {0} turn...", player.Name);
            Console.WriteLine();
        }

        /// <summary>
        /// Gets the play decision.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns>
        /// the <see cref="BlackjackPlayerDecision"/>
        /// </returns>
        public BlackjackPlayerDecision GetPlayDecision(IGamePlayer player)
        {
            do
            {
                Console.WriteLine("Please select Hit (H) or Stand (S)...");
                string decision = Console.ReadLine().Trim().ToUpper();

                if (decision == "HIT" || decision == "H")
                    return BlackjackPlayerDecision.Hit;

                if (decision == "STAND" || decision == "S")
                    return BlackjackPlayerDecision.Stand;

                Console.WriteLine("Invalid choice!!!");
                Console.WriteLine();
            } while (true);
        }
    }
}
